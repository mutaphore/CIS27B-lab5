
public class Agent {

	private String name;
	private Client[] my_Clients;
	private int num_Clients;
	public static final int MAX_CLIENTS = 500;
	public static final int MIN_NAME_LEN = 0;
	public static final int MAX_NAME_LEN = 100;
	public static final String DEFAULT_NAME = "ANONYMOUS";
	
	public Agent() {
		
		name = DEFAULT_NAME;
		my_Clients = new Client[MAX_CLIENTS]; //Create array size of MAX_CLIENTS
		num_Clients = 0;
		
	}
	
	public Agent(String name) {
		
		if(!SetName(name))
			name = DEFAULT_NAME;
		my_Clients = new Client[MAX_CLIENTS];
		num_Clients = 0;
			
	}
	
	//Accessors---------------------------------------
	
	public String GetName() {
		
		return name;
		
	}
	
	//Mutators---------------------------------------
	public boolean SetName(String name) {
		
		if(MIN_NAME_LEN <= name.length() && name.length() <= MAX_NAME_LEN) {
			this.name = name;
			return true;
		}
		else
			return false;
		
	}
	
	public void AddClient(Client client) {
		
		num_Clients++; //Increment num_clients by 1 after adding
		
		my_Clients[num_Clients-1] = client; //add to last member
		
	}
	
	public void RemoveClient(Client client) {
		
		for(int i = 0; i < num_Clients; i++) {
			
			if(my_Clients[i] == client) {
				
				for(int j = i; j < num_Clients-1 ; j++)
					my_Clients[j] = my_Clients[j+1]; //Shift array cells over
				
				i = num_Clients; //force end of loop
				
			}
		
		}
		
		num_Clients--; //Decrement num_clients by 1 after removing
		
	}
	
	//Helpers--------------------------------------
	
	public void ShowClientsShort() {
		
		System.out.println("Agent " + name + " client list (Short): ");
		
		for(int i = 0; i < num_Clients; i++)
			System.out.println((i+1) + ": " + my_Clients[i].GetName());
		
	}
	
	public void ShowClientsLong() {
		
		System.out.println("Agent " + name + " client list (Long): ");
		
		for(int i = 0; i < num_Clients; i++) {
		
			System.out.println((i+1) + ": "); 
			my_Clients[i].Display();
		
		}
			
	}
	
	public double GetIncome() {
		
		double sum = 0;
		
		for(int i = 0; i < num_Clients; i++)
			sum += my_Clients[i].GetPercentCut() * my_Clients[i].GetIncome();
			
		return sum;
		
	}
	
	
	
}
