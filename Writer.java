
public class Writer extends Client {

	public boolean technical;
	public boolean government;
	public boolean international;
	private int rank;
	public static final int DEFAULT_RANK = 1;
	public static final int MAX_RANK = 6;
	public static final int MIN_RANK = 1;
	public static final String RANK1 = "staff writer";
	public static final String RANK2 = "story editor";
	public static final String RANK3 = "co-producer";
	public static final String RANK4 = "producer";
	public static final String RANK5 = "co-executive producer";
	public static final String RANK6 = "executive producer";
	
	//Zero parameter constructor
	public Writer() {
		
		super();
		technical = false;
		government = false;
		international = false;
		rank = DEFAULT_RANK;
		
	}
	
	//This contructor initialize just this derived class
	public Writer(boolean technical, boolean government, boolean international, int rank) {
		
		super();
		this.technical = technical;
		this.government = government;
		this.international = international;
		if(!SetRank(rank))
			rank = DEFAULT_RANK;

	}
	
	//This contructor initializes base class too
	public Writer(String name, long income_this_year, double percent_cut, boolean technical, 
					  boolean government, boolean international, int rank) {
		
		super(name, income_this_year, percent_cut);
		this.technical = technical;
		this.government = government;
		this.international = international;
		if(!SetRank(rank))
			rank = DEFAULT_RANK;

	}
	
	//Accessors---------------------------------------
	
	public boolean GetTechnical() {
		
		return technical;
		
	}
	
	public boolean GetGovernment() {
		
		return government;
		
	}
	
	public boolean GetInternational() {
		
		return international;
	}
	
	//Mutators---------------------------------------
	public boolean SetRank(int rank) {
		
		if(MIN_RANK <= rank && rank <= MAX_RANK) {
			this.rank = rank;
			return true;
		}
		else
			return false;
		
	}
	
	//Helpers-----------------------------------------
	
	public String ToStringRank() {
		
		String rank_string = new String();
		
		switch (rank) {
			case 1:	rank_string = RANK1;
						break;
			case 2: 	rank_string = RANK2;
						break;
			case 3: 	rank_string = RANK3;
						break;
			case 4: 	rank_string = RANK4;
						break;
			case 5: 	rank_string = RANK5;
						break;
			case 6: 	rank_string = RANK6;
						break;
		}
		
		return rank_string;
		
	}
	
	
	
}
