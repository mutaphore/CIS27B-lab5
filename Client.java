
public class Client {
	
	protected String name;
	protected long income_this_year;
	protected double percent_cut;
	public static final long MIN_INCOME_VAL = 0;
	public static final long MAX_INCOME_VAL = 99999999;
	public static final int MIN_NAME_LEN = 0;
	public static final int MAX_NAME_LEN = 100;
	public static final double MIN_PERCENT_VAL = Double.MIN_VALUE; //minimum double
	public static final double MAX_PERCENT_VAL = 1.0;
	public static final String DEFAULT_NAME = "ANONYMOUS";
	public static final long DEFAULT_INCOME = 30000;
	public static final double DEFAULT_PERCENT = 0.1;
	
	public Client(){
		
		name = DEFAULT_NAME;
		income_this_year = DEFAULT_INCOME;
		percent_cut = DEFAULT_PERCENT;
		
	}
	
	public Client(String name, long income_this_year, double percent_cut) {
		
		if(!SetName(name))
			name = DEFAULT_NAME;
		if(!SetIncome(income_this_year));
			income_this_year = DEFAULT_INCOME;
		if(!SetPercentCut(percent_cut))
			percent_cut = DEFAULT_PERCENT;
		
	}
	
	//Accessors----------------------------------------
	
	public String GetName() {
		
		return name;
		
	}
	
	public long GetIncome() {
		
		return income_this_year;
	}
	
	public double GetPercentCut() {
		
		return percent_cut;
		
	}
	
	//Mutators----------------------------------------
	
	public boolean SetName(String name) {
		
		if(MIN_NAME_LEN <= name.length() && name.length() <= MAX_NAME_LEN) {
			this.name = name;
			return true;
		}
		else
			return false;
		
	}
	
	public boolean SetIncome(long income_this_year) {
		
		if(MIN_INCOME_VAL <= income_this_year && income_this_year <= MAX_INCOME_VAL) {
			this.income_this_year = income_this_year;
			return true;
		}
		else
			return false;
		
	}
	
	public boolean SetPercentCut(double percent_cut) {
	
		if(MIN_PERCENT_VAL <= percent_cut && percent_cut <= MAX_PERCENT_VAL) {
			this.percent_cut = percent_cut;
			return true;
		}
		else
			return false;
		
	}
	
	//Helpers-------------------------------------------
	
	public void Display(){
		
		System.out.println("Name: " + name);
		System.out.println("This year's income: $" + income_this_year);
		System.out.println("Percent cut: " + percent_cut);
		System.out.println();		
		
	}

	
}
