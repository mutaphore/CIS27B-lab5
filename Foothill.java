
public class Foothill {


	public static void main(String[] args) {
		
		//Instantiate some writers, actors and a agent using different constructors
		Agent agent = new Agent("Andrew"); 
		
		Writer w1 = new Writer();
		w1.SetName("Jessica");
		w1.SetIncome(60000);
		w1.SetPercentCut(0.07);
		w1.government = true;
		w1.technical = false;
		w1.international = true;
		w1.SetRank(4);
		
		Writer w2 = new Writer(false, false, true, 2);
		w2.SetName("Tracey");
		w2.SetIncome(34000);
		w2.SetPercentCut(0.11);
		
		Writer w3 = new Writer("David", 40000, 0.3, false, true, false, 3);
		
		Actor a1 = new Actor();
		a1.SetName("Jack");
		a1.SetIncome(55000);
		a1.SetPercentCut(0.13);
		a1.SetAge(30);
		a1.SetGender('M');
		
		Actor a2 = new Actor('F', 26);
		a2.SetName("Morgan");
		a2.SetIncome(73000);
		a2.SetPercentCut(0.18);
		
		Actor a3 = new Actor("Kevin", 65000, 0.2, 'M', 28);
		
		agent.AddClient(w1);
		agent.AddClient(w2);
		agent.AddClient(w3);
		agent.AddClient(a1);
		agent.AddClient(a2);
		agent.RemoveClient(a2);
		agent.AddClient(a2);
		agent.AddClient(a3);
		
		//Display some actors and writers data individually
		System.out.println("-----Display some actors and writers data individually-----\n");
		System.out.println("Writer: ");
		System.out.println(w1.GetName());
		System.out.println(w1.GetIncome());
		System.out.println(w1.GetPercentCut());
		System.out.println("Government: " + w1.GetGovernment());
		System.out.println("International: " + w1.GetInternational());
		System.out.println("Technical: " + w1.GetTechnical());
		System.out.println("Rank: " + w1.ToStringRank());
		
		System.out.println();
		
		System.out.println("Actor: ");
		System.out.println(a1.GetName());
		System.out.println(a1.GetIncome());
		System.out.println(a1.GetPercentCut());
		System.out.println(a1.GetAge());
		System.out.println(a1.GetGender());
		
		//Display using ShowClients()
		System.out.println("\n-----Display using ShowClients()-----\n");

		agent.ShowClientsShort();
		System.out.print("\n");
		agent.ShowClientsLong();
		
		System.out.println("Agent " + agent.GetName() + "'s income this year: $" + agent.GetIncome());
		
	}

}
